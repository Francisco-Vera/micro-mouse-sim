package Objects;

import java.awt.*;


public class Mouse {
    public int x,y;
    public enum Direction{
    	NORTH,
    	SOUTH,
    	EAST,
    	WEST
    }
    public Direction facing;

    public Mouse(int x, int y, Direction facing) {
        this.x = x;
        this.y = y;
        this.facing = facing;
    }

    public void render(Graphics g) {
        g.setColor(Color.BLUE);
        g.fillRect(9 + 32*x, 9 + 32*y, 24, 24);
        g.setColor(Color.RED);
        if(this.facing == Direction.NORTH) {
        	g.fillRect(14 + 32*x, 10 +32*y, 4, 4);
        	g.fillRect(24 + 32*x, 10 +32*y, 4, 4);
        }
        if(this.facing == Direction.WEST) { 
        	g.fillRect(10 + 32*x, 14 +32*y, 4, 4);
        	g.fillRect(10 + 32*x, 24 +32*y, 4, 4);
        }
        if(this.facing == Direction.EAST) {
        	g.fillRect(28 + 32*x, 14 +32*y, 4, 4);
        	g.fillRect(28 + 32*x, 24 +32*y, 4, 4);
        }
        if(this.facing == Direction.SOUTH) { 
        	g.fillRect(14 + 32*x, 28 +32*y, 4, 4);
        	g.fillRect(24 + 32*x, 28 +32*y, 4, 4);
        }
        
        
    }
}
