package Objects;

import java.awt.*;


public class Cell {
    public boolean n,s,e,w;
    int x,y,distance;

    public Cell(boolean n, boolean s, boolean e, boolean w, int x, int y, int distance) {
        this.n = n;
        this.s = s;
        this.e = e;
        this.w = w;
        this.x = x;
        this.y = y;
        this.distance = 0;
    }
    
    public int getDistance() {
    	return this.distance;
    }
    
    public void setDistance(int distance) {
    	this.distance = distance;
    }
    

    public boolean getN() {
		return n;
	}

	public void setN(boolean n) {
		this.n = n;
	}

	public boolean getS() {
		return s;
	}

	public void setS(boolean s) {
		this.s = s;
	}

	public boolean getE() {
		return e;
	}

	public void setE(boolean e) {
		this.e = e;
	}

	public boolean getW() {
		return w;
	}

	public void setW(boolean w) {
		this.w = w;
	}

	public void render(Graphics g) {
        g.setColor(Color.WHITE);
        //g.drawRect((x*32)+1,(y*32)+1,32,32);
        g.drawString("" + distance, x*32 + 16 , y*32 + 24);
        
        if(this.n == true) {
        	g.setColor(Color.WHITE);
        	g.drawLine((x*32)+5, (y*32)+5, (x*32)+37 , (y*32)+5);
        }
        if(this.w == true) {
        	g.setColor(Color.WHITE);
        	g.drawLine((x*32)+5, (y*32)+5, (x*32)+5 , (y*32)+37);
        	}
        if(this.e == true) {
        	g.setColor(Color.WHITE);
        	g.drawLine((x*32)+37, (y*32)+37, (x*32)+37 , (y*32)+5);
        }
        if(this.s == true) {
        	g.setColor(Color.WHITE);
        	g.drawLine((x*32)+5, (y*32)+37, (x*32)+37 , (y*32)+37);
        }
        g.setColor(Color.GRAY);
        g.drawString("" + distance, x*32 + 16 , y*32 + 24);
    }
}
