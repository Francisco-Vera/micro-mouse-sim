package Objects;

import java.awt.*;
import java.util.Stack;

import GameStates.State;
import Objects.Mouse.Direction;


public class Maze {
	public Cell walls[][] = new Cell[16][16];
	public Mouse mouse;
	public Stack<Cell> stack = new Stack<Cell>();;

	public Maze(Cell[][] walls,Mouse mouse) {
		this.walls = walls;
		this.mouse = mouse;

	}

	public void generateDefaultDistances(Cell[][] walls) { //generates distances from the center of the maze outwards, as if there were no walls
		int distance = 14;
		int offset = 0;

		for(int x = 0; x <= 8;x++) {
			distance = 14 - x;
			for(int  y = 0; y <= 7; y++ ) {
				walls[x][y].setDistance(distance--);
			}
		}
		distance = 14;
		for(int x = 15; x >= 8;x--) {
			distance = 14 - offset++ ;
			for(int  y = 0; y <= 7; y++ ) {
				walls[x][y].setDistance(distance--);
			}
		}
		distance = 14;
		for(int x = 0; x <= 8;x++) {
			distance = 14 - x;
			for(int  y = 15; y >= 8; y-- ) {
				walls[x][y].setDistance(distance--);
			}
		}
		distance = 14;
		offset = 0;
		for(int x = 15; x >= 8;x--) {
			distance = 14 - offset++;
			for(int  y = 15; y >= 8; y-- ) {
				walls[x][y].setDistance(distance--);
			}
		}
	}

	public void updateDistances() { // meant to update the distances the mouse discovers the walls
		Cell currentCell = this.walls[this.mouse.x][this.mouse.y];
		stack.add(currentCell);
//		if(currentCell.e && this.getEastCell(currentCell)!= null) {
//			stack.add(this.getEastCell(currentCell));
//		}
//		if(currentCell.w && this.getWestCell(currentCell)!= null) {
//			stack.add(this.getWestCell(currentCell));
//		}
//		if(currentCell.n && this.getNorthCell(currentCell)!= null) {
//			stack.add(this.getNorthCell(currentCell));
//		}
//		if(currentCell.s && this.getSouthCell(currentCell)!= null) {
//			stack.add(this.getSouthCell(currentCell));
//		}

		while(!stack.empty()) {
			Cell c = stack.pop();
			int value = c.distance;
			int lowestNeighbor = this.getLowestAccecibleNeighbor(c);
//			if(this.getEastCell(c) != null) {
//				lowestNeighbor = this.getEastCell(c).distance;
//			}
//			if(this.getWestCell(c) !=null) {
//				if(lowestNeighbor == 0 ||this.getWestCell(c).distance < lowestNeighbor ) {
//					lowestNeighbor = this.getWestCell(c).distance;
//				}
//			}
//			if(this.getNorthCell(c) !=null) {
//				if(lowestNeighbor == 0 ||this.getNorthCell(c).distance < lowestNeighbor ) {
//					lowestNeighbor = this.getNorthCell(c).distance;
//				}
//			}
//			if(this.getSouthCell(c) !=null) {
//				if(lowestNeighbor == 0 ||this.getSouthCell(c).distance < lowestNeighbor ) {
//					lowestNeighbor = this.getSouthCell(c).distance;
//				}
//			}
			if(value != lowestNeighbor +1 && lowestNeighbor != 0) {
				c.distance = lowestNeighbor +1;
				if(!c.e && this.getEastCell(c)!= null) {
					stack.add(this.getEastCell(c));
				}
				if(!c.w && this.getWestCell(c)!= null) {
					stack.add(this.getWestCell(c));
				}
				if(!c.n && this.getNorthCell(c)!= null) {
					stack.add(this.getNorthCell(c));
				}
				if(c.s && this.getSouthCell(c)!= null) {
					stack.add(this.getSouthCell(c));
				}
			}
			
		}

	}

	public void move() {
		
		if (this.mouse.facing == Direction.EAST) {
			if(this.walls[this.mouse.x][this.mouse.y].e == false &&
					this.walls[this.mouse.x][this.mouse.y].getDistance() 
					== this.walls[this.mouse.x+1][this.mouse.y].getDistance() + 1
					) {
				this.mouse.x = this.mouse.x + 1;
			}else if(this.walls[this.mouse.x][this.mouse.y].n == false) {
				this.mouse.facing = Direction.NORTH;
				return;
			}else if(this.walls[this.mouse.x][this.mouse.y].w == false) {
				this.mouse.facing = Direction.WEST;
				return;
			}else if(this.walls[this.mouse.x][this.mouse.y].s == false) {
				this.mouse.facing = Direction.SOUTH;
				return;
			}
		}
		if (this.mouse.facing == Direction.WEST) {
			if(this.walls[this.mouse.x][this.mouse.y].w == false &&
					this.walls[this.mouse.x][this.mouse.y].getDistance() 
					== this.walls[this.mouse.x-1][this.mouse.y].getDistance() + 1
					) {
				this.mouse.x = this.mouse.x - 1;
			}else if(this.walls[this.mouse.x][this.mouse.y].s == false) {
				this.mouse.facing = Direction.SOUTH;
				return;
			}else if(this.walls[this.mouse.x][this.mouse.y].n == false) {
				this.mouse.facing = Direction.NORTH;
				return;
			}else if(this.walls[this.mouse.x][this.mouse.y].e == false) {
				this.mouse.facing = Direction.EAST;
				return;
			}
		}
		if (this.mouse.facing == Direction.NORTH) {
			if(this.walls[this.mouse.x][this.mouse.y].n == false &&
					this.walls[this.mouse.x][this.mouse.y].getDistance() 
					== this.walls[this.mouse.x][this.mouse.y-1].getDistance() + 1
					) {
				this.mouse.y = this.mouse.y - 1;
			}else if(this.walls[this.mouse.x][this.mouse.y].w == false) {
				this.mouse.facing = Direction.WEST;
				return;
			}else if(this.walls[this.mouse.x][this.mouse.y].e == false) {
				this.mouse.facing = Direction.EAST;
				return;
			}else if(this.walls[this.mouse.x][this.mouse.y].s == false) {
				this.mouse.facing = Direction.SOUTH;
				return;
			}
		}
		if (this.mouse.facing == Direction.SOUTH) {
			if(this.walls[this.mouse.x][this.mouse.y].s == false &&
					this.walls[this.mouse.x][this.mouse.y].getDistance() 
					== this.walls[this.mouse.x][this.mouse.y+1].getDistance() + 1
					) {
				this.mouse.y = this.mouse.y + 1;
			}else if(this.walls[this.mouse.x][this.mouse.y].e == false) {
				this.mouse.facing = Direction.EAST;
				return;
			}else if(this.walls[this.mouse.x][this.mouse.y].w == false) {
				this.mouse.facing = Direction.WEST;
				return;
			}else if(this.walls[this.mouse.x][this.mouse.y].n == false) {
				this.mouse.facing = Direction.NORTH;
				return;
			}
		}
	}
	
	public int getLowestAccecibleNeighbor(Cell c) {
		
		int EastValue, WestValue, NorthValue, SouthValue;
		
		if(c.e == false) {
			EastValue = this.getEastCell(c).distance;
		}
		else {
			EastValue = 255;
		}
		if(c.w == false) {
			WestValue = this.getWestCell(c).distance;
		}
		else {
			WestValue = 255;
		}
		if(c.n == false) {
			NorthValue = this.getNorthCell(c).distance;
		}
		else {
			NorthValue = 255;
		}
		if(c.s == false) {
			SouthValue = this.getSouthCell(c).distance;
		}
		else {
			SouthValue = 255;
		}
		
		int lowestValue = EastValue;
		
		if(WestValue < lowestValue) {
			lowestValue = WestValue;
		}
		if(NorthValue < lowestValue) {
			lowestValue = NorthValue;
		}
		if(SouthValue < lowestValue) {
			lowestValue = SouthValue;
		}
		
		return lowestValue;
	}


	public void setE(int x,int y) {
		walls[x][y].setE(true);
		walls[x+1][y].setW(true);
	}

	public void setW(int x,int y) {
		walls[x][y].setW(true);
		walls[x-1][y].setE(true);
	}
	public void setN(int x,int y) {
		walls[x][y].setN(true);
		walls[x][y-1].setS(true);

	}
	public void setS(int x,int y) {
		walls[x][y].setS(true);
		walls[x][y+1].setN(true);
	}

	public void setColumnE(int x, int[] column) { // Places eastern walls in a column with respect to a parameter array
		for(int i = 0;i<column.length;i++) {
			setE(x,column[i]);
		}
	}

	public void setColumnS(int x, int[] column) { // Places southern walls in a column with respect to a parameter array
		for(int i = 0;i<column.length;i++) {
			setS(x,column[i]);
		}
	}

	public Cell getNorthCell(Cell c) {
		try{
			return this.walls[c.x][c.y-1];
		}
		catch (Exception e){
			return null;
		}
	}
	public Cell getSouthCell(Cell c) {
		try{
			return this.walls[c.x][c.y+1];
		}
		catch (Exception e){
			return null;
		}
	}
	public Cell getWestCell(Cell c) {
		try{
			return this.walls[c.x-1][c.y];
		}
		catch (Exception e){
			return null;
		}
	}
	public Cell getEastCell(Cell c) {
		try{
			return this.walls[c.x+1][c.y];
		}
		catch (Exception e){
			return null;
		}
	}

	public void render(Graphics g) {
		for (int x = 0; x < 16; x++) {
			for (int y = 0; y < 16; y++) {
				walls[x][y].render(g);
			}
		}
		mouse.render(g);
	}
}
