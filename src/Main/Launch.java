package Main;

/**
 * Created by AlexVR on 1/24/2020.
 */

public class Launch {

    public static void main(String[] args) {
        GameSetUp game = new GameSetUp("Micromouse Simulator", 700, 524);
        game.start();
    }
}
