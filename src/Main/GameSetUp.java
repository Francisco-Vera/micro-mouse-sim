package Main;

import Display.DisplayScreen;

import GameStates.GameState;
import GameStates.State;

import java.awt.*;
import java.awt.image.BufferStrategy;


/**
 * Created by AlexVR on 1/24/2020.
 */

public class GameSetUp implements Runnable {
    private DisplayScreen display;
    public int width, height;
    private String title;

    private boolean running = false;
    private Thread thread;

    //Input
    public MouseManager mouseManager;

    //Handler

    //States
    public State gameState;

    GameSetUp(String title, int width, int height){

        this.width = width;
        this.height = height;
        this.title = title;
        mouseManager = new MouseManager();

    }

    private void init(){
        display = new DisplayScreen(title, width, height);
        display.getFrame().addMouseListener(mouseManager);
        display.getFrame().addMouseMotionListener(mouseManager);
        display.getCanvas().addMouseListener(mouseManager);
        display.getCanvas().addMouseMotionListener(mouseManager);

        //checks for all images to exist
        Images img = new Images();



        gameState = new GameState(this);

        State.setState(gameState);
    }


    public void reStart(boolean clearScore){
        gameState = new GameState(this);

        State.setState(gameState);

    }

     synchronized void start(){
        if(running)
            return;
        running = true;
        //this runs the run method in this  class
        thread = new Thread(this);
        thread.start();
    }

    public void run(){

        //initializes everything in order to run without breaking
        init();

        int fps = 60;
        double timePerTick = 1000000000 / fps;
        double delta = 0;
        long now;
        long lastTime = System.nanoTime();
        long timer = 0;
        int ticks = 0;

        while(running){
            //makes sure the games runs smoothly at 60 FPS
            now = System.nanoTime();
            delta += (now - lastTime) / timePerTick;
            timer += now - lastTime;
            lastTime = now;

            if(delta >= 1){
                //re-renders and ticks the game around 60 times per second
                tick();
                render();
                ticks++;
                delta--;
            }

            if(timer >= 1000000000){
                ticks = 0;
                timer = 0;
            }
        }

        stop();

    }

    private void tick(){
        //checks for key types and manages them



        //game states are the menus
        if(State.getState() != null)
            State.getState().tick();
    }

    private void render(){
        BufferStrategy bs = display.getCanvas().getBufferStrategy();
        if(bs == null){
            display.getCanvas().createBufferStrategy(3);
            return;
        }
        Graphics g = bs.getDrawGraphics();
        //Clear Screen
        g.clearRect(0, 0, width, height);

        //Draw Here!
        if(State.getState() != null)
            State.getState().render(g);


        //End Drawing!
        bs.show();
        g.dispose();
    }

    private synchronized void stop(){
        if(!running)
            return;
        running = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    MouseManager getMouseManager(){
        return mouseManager;
    }

}

