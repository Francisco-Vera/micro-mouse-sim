package Main;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Created by AlexVR on 1/24/2020.
 */
public class Images {


    public static BufferedImage[] startGameButton;
    public static BufferedImage titleScreenBackground, number1, number2, number3,
    up, down, pause, play, fast, slow, step, blank, back;


    public Images() {

        startGameButton = new BufferedImage[3];

        try {

            startGameButton[0]= ImageIO.read(getClass().getResourceAsStream("/UI/Buttons/Start/NormalStartButton.png"));
            startGameButton[1]= ImageIO.read(getClass().getResourceAsStream("/UI/Buttons/Start/HoverStartButton.png"));
            startGameButton[2]= ImageIO.read(getClass().getResourceAsStream("/UI/Buttons/Start/ClickedStartButton.png"));

            titleScreenBackground = ImageIO.read(getClass().getResourceAsStream("/UI/Backgrounds/Title.png"));
            number1 = ImageIO.read(getClass().getResourceAsStream("/UI/Buttons/number1.png"));
            number2 = ImageIO.read(getClass().getResourceAsStream("/UI/Buttons/number2.png"));
            number3 = ImageIO.read(getClass().getResourceAsStream("/UI/Buttons/number3.png"));
            up = ImageIO.read(getClass().getResourceAsStream("/UI/Buttons/up.png"));
            down = ImageIO.read(getClass().getResourceAsStream("/UI/Buttons/down.png"));
            pause = ImageIO.read(getClass().getResourceAsStream("/UI/Buttons/pause.png"));
            play = ImageIO.read(getClass().getResourceAsStream("/UI/Buttons/play.png"));
            fast = ImageIO.read(getClass().getResourceAsStream("/UI/Buttons/fast.png"));
            slow = ImageIO.read(getClass().getResourceAsStream("/UI/Buttons/slow.png"));
            step = ImageIO.read(getClass().getResourceAsStream("/UI/Buttons/step.png"));
            blank = ImageIO.read(getClass().getResourceAsStream("/UI/Buttons/blank.png"));
            back = ImageIO.read(getClass().getResourceAsStream("/UI/Buttons/back.jpg"));
        }catch (IOException e) {
            e.printStackTrace();
        }

    }


}
