package GameStates;


import java.awt.*;
import java.awt.event.MouseMotionListener;

/**
 * Created by AlexVR on 7/1/2018.
 */
public abstract class State {

    private static State currentState = null;

    public static void setState(State state){
        currentState = state;
    }
    

    public static State getState(){
        return currentState;
    }
    
    public static GameState getGameState() {
    	return (GameState) currentState;
    }

    //CLASS


    public State(){
    }

    public abstract void tick();

    public abstract void render(Graphics g);


}

