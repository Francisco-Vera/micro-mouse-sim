package GameStates;

import Objects.Cell;
import Objects.Maze;
import Objects.Mouse;
import Objects.Mouse.Direction;

import java.awt.*;
import java.awt.image.BufferedImage;

import Display.UI.ClickListlener;
import Display.UI.UIImageButton;
import Display.UI.UIManager;
import Main.GameSetUp;
import Main.Images;
import Main.MouseManager;


public class GameState extends State {

	public Maze maze;
	int prevSelection = 1;
	int selection = 1;
	int count = 0;
	int delay = 8;
	boolean paused = true;
	boolean changed = false;
	private UIManager uiManager;
	MouseManager mouseManager;
	GameSetUp setup;

	public GameState(GameSetUp main){
		super();
		setup = main;
		mouseManager = main.mouseManager;
		uiManager = new UIManager();
		mouseManager.setUimanager(uiManager);

		uiManager.addObjects(new UIImageButton(584, 50, 54, 66, Images.blank, new ClickListlener() {
			@Override
			public void onClick() { // Pause/Play button, runs if the game is paused, pauses if the game is running
				mouseManager.setUimanager(null);
				if(paused) { 
					paused = false;
				}else {
					paused = true;
				}

			}
		}));
		uiManager.addObjects(new UIImageButton(584, 170, 54, 66, Images.blank, new ClickListlener() {
			@Override
			public void onClick() { // Manual movement button, only available when simulation is paused
				mouseManager.setUimanager(null);
				if(paused) { 
					maze.move();
				}
			}
		}));
		uiManager.addObjects(new UIImageButton(610, 130, 65, 55, Images.fast, new ClickListlener() {
			@Override
			public void onClick() { // Fast forward button
				mouseManager.setUimanager(null);
				if(delay !=0) {
					count = 0;
					delay -=8;
				}
			}
		}));
		uiManager.addObjects(new UIImageButton(544, 130, 65, 55, Images.slow, new ClickListlener() {
			@Override
			public void onClick() { // Slow down button
				mouseManager.setUimanager(null);
				if(delay != 16) {
					count = 0;
					delay +=8;;
				}
			}
		}));
		uiManager.addObjects(new UIImageButton(584, 290, 54, 42, Images.up, new ClickListlener() {
			@Override
			public void onClick() { // Up button, used to switch to a maze with a higher number
				mouseManager.setUimanager(null);
				if(selection < 3) selection++;
			}
		}));
		uiManager.addObjects(new UIImageButton(584, 380, 54, 42, Images.down, new ClickListlener() {
			@Override
			public void onClick() { // Down button, used to switch to a maze with a lower number
				mouseManager.setUimanager(null);
				if(selection > 1) selection--;
			}
		}));

		Cell[][] walls = new Cell[16][16];
		maze = new Maze(walls, new Mouse(0,0, Direction.NORTH));
		setMaze1(maze);

	}


	@Override
	public void tick() {
		if(maze.walls[maze.mouse.x][maze.mouse.y].getDistance() == 0) {
			this.paused = true;
			System.out.println("You made it!");
		}
		mouseManager.setUimanager(uiManager);
		uiManager.tick();
		if(!paused) { // Only moves is the simulator isn't paused
			
			if(count!=delay) {
				count++;
			}else {
				maze.move();
				maze.updateDistances();
				count=0;
			}
		}
		
		changed = prevSelection != selection; // Checks if the selection has been changed
		if(changed) { // Changes the maze based on the new selection
			switch(selection) {
			case(1):
				setMaze1(maze);
			break;
			case(2):
				setMaze2(maze);
			break;
			case(3):
				setmaze(maze);
			break;
			}
			prevSelection = selection; // Next tick will check if the selection is equal to the one it just had
			changed = false; // Need to set changed to false, otherwise, the mazes would continuously restart
		}
	}



	@Override
	public void render(Graphics g) {
		uiManager.Render(g);
		maze.render(g);
		
		if(paused) { // If statement used to change between the pause and play button
			g.drawImage(Images.play, 588, 50, 54, 66, null);
			g.drawImage(Images.step, 570, 200, 84, 60, null);
		}else {
			g.drawImage(Images.pause, 576, 50, 66, 66, null);
		}
		
		switch(selection) { // Changes the display number according to the current selection
		case(1):
			g.drawImage(Images.number1, 584, 320, 54, 66, null);
			break;
		case(2):
			g.drawImage(Images.number2, 584, 320, 54, 66, null);
			break;
		case(3):
			g.drawImage(Images.number3, 584, 320, 54, 66, null);
			break;
		}

	}

	public Cell[][] clearWalls(Cell[][] wall) { // Clears the inner walls of a maze
		for (int x = 0; x < 16; x++) {
			for (int y = 0; y < 16; y++) {
				if(x==0) {
					wall[x][y] = new Cell(false,false,false,true,x,y,0);
				}
				else if(y==0) {
					wall[x][y] = new Cell(true,false,false,false,x,y,0);
				}
				else if(x==15) {
					wall[x][y] = new Cell(false,false,true,false,x,y,0);
				}
				else if(y==15) {
					wall[x][y] = new Cell(false,true,false,false,x,y,0);
				}
				else{
					wall[x][y] = new Cell(false,false,false,false,x,y,0);
				}
			}
		}
		wall[0][0].setN(true);
		wall[0][0].setW(true);
		wall[0][15].setW(true);
		wall[0][15].setS(true);
		wall[15][15].setE(true);
		wall[15][15].setS(true);
		wall[15][0].setE(true);
		wall[15][0].setN(true);	

		return wall;
	}

	public void setMaze1(Maze maze)
	{
		paused = true; 
		maze.mouse = new Mouse(0,0,Direction.NORTH); 
		maze = new Maze(clearWalls(maze.walls), maze.mouse);
		maze.generateDefaultDistances(maze.walls);
		int[] column0E = new int[]{0,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
		int[] column1E = new int[]{};
		int[] column2E = new int[]{1,12,14};
		int[] column3E = new int[]{2,4,5,7,8,9,10,13,14};
		int[] column4E = new int[]{2,3,6,7,8,11,12,14};
		int[] column5E = new int[]{3,4,5,6,10,11};
		int[] column6E = new int[]{7,8,9,12,13};
		int[] column7E = new int[]{10,14};
		int[] column8E = new int[]{3,4,5,7,8,9,13};
		int[] column9E = new int[]{2,3,6,10,11,12};
		int[] column10E = new int[]{4,5,6,7,8,9,10};
		int[] column11E = new int[]{1,3,4,5,10,11,12,13};
		int[] column12E = new int[]{1,2,4,6,7,8,9,12,13,14};
		int[] column13E = new int[]{0,1,5,6,7,8,9,10,11,12,14};
		int[] column14E = new int[]{1,2,3,4,5,6,7,8,9,10,11,12,13};

		int[] column0S = new int[]{};
		int[] column1S = new int[]{0,1,2,3,4,7,9};
		int[] column2S = new int[]{1,5,6,7,8,9,10,11,13};
		int[] column3S = new int[]{0,2,3,4,6,8,10,11,13};
		int[] column4S = new int[]{0,3,5,9,11,14};
		int[] column5S = new int[]{0,1,4,7,9,12,14};
		int[] column6S = new int[]{0,1,2,3,4,5,6,8,10,12,13,14};
		int[] column7S = new int[]{0,1,8,10,12,14};
		int[] column8S = new int[]{0,1,2,3,4,5,6,8,10,11,12,14};
		int[] column9S = new int[]{0,1,4,6,8,10,13,14};
		int[] column10S = new int[]{0,1,2,4,7,9,11,13,14};
		int[] column11S = new int[]{0,2,6,8,12,14};
		int[] column12S = new int[]{1,5,9,10,14};
		int[] column13S = new int[]{2,3,4,11};
		int[] column14S = new int[]{2,13,14};
		int[] column15S = new int[]{};

		maze.setColumnE(0, column0E);
		maze.setColumnE(1, column1E);
		maze.setColumnE(2, column2E);
		maze.setColumnE(3, column3E);
		maze.setColumnE(4, column4E);
		maze.setColumnE(5, column5E);
		maze.setColumnE(6, column6E);
		maze.setColumnE(7, column7E);
		maze.setColumnE(8, column8E);
		maze.setColumnE(9, column9E);
		maze.setColumnE(10, column10E);
		maze.setColumnE(11, column11E);
		maze.setColumnE(12, column12E);
		maze.setColumnE(13, column13E);
		maze.setColumnE(14, column14E);


		maze.setColumnS(0, column0S);
		maze.setColumnS(1, column1S);
		maze.setColumnS(2, column2S);
		maze.setColumnS(3, column3S);
		maze.setColumnS(4, column4S);
		maze.setColumnS(5, column5S);
		maze.setColumnS(6, column6S);
		maze.setColumnS(7, column7S);
		maze.setColumnS(8, column8S);
		maze.setColumnS(9, column9S);
		maze.setColumnS(10, column10S);
		maze.setColumnS(11, column11S);
		maze.setColumnS(12, column12S);
		maze.setColumnS(13, column13S);
		maze.setColumnS(14, column14S);
		maze.setColumnS(15, column15S);
	}

	private void setMaze2(Maze maze) {
		paused = true;
		maze.mouse = new Mouse(0,0,Direction.NORTH);
		maze = new Maze(clearWalls(maze.walls), maze.mouse);
		maze.generateDefaultDistances(maze.walls);
		int[] column0E = new int[]{1,2,3,4,5,6,7,8,9,10,11,12,14,15};
		int[] column1E = new int[]{2,3,4,5,6,7,8,9,10,11,12,13,14};
		int[] column2E = new int[]{3,4,5,8,9,10,13};
		int[] column3E = new int[]{3,7};
		int[] column4E = new int[]{4,5,6,8,9};
		int[] column5E = new int[]{3,4,5,6,7,8,10,11,12};
		int[] column6E = new int[]{5,6,7,8,9,11,12,13,14};
		int[] column7E = new int[]{4,10,11,12,13};
		int[] column8E = new int[]{2,3,5,8,10};
		int[] column9E = new int[]{3,4,7,9,11};
		int[] column10E = new int[]{4,5,6,10,12};
		int[] column11E = new int[]{3,8,9,11,13,14};
		int[] column12E = new int[]{9,12};
		int[] column13E = new int[]{6,7,9,11,13};
		int[] column14E = new int[]{2,3,4,5,6,7,8,9,10,11,12,13,14};

		int[] column0S = new int[]{};
		int[] column1S = new int[]{0};
		int[] column2S = new int[]{0,1,6,11,14};
		int[] column3S = new int[]{0,1,2,5,6,8,10,11,12,13,14};
		int[] column4S = new int[]{0,1,2,4,6,7,9,10,11,12,13,14};
		int[] column5S = new int[]{0,1,2,10,12,13,14};
		int[] column6S = new int[]{0,1,3,9,14};
		int[] column7S = new int[]{0,1,2,3,5,6,8,9,14};
		int[] column8S = new int[]{0,1,2,4,6,8,11,12,13,14};
		int[] column9S = new int[]{0,1,2,5,6,8,10,12,13,14};
		int[] column10S = new int[]{0,2,5,7,9,11,13,14};
		int[] column11S = new int[]{0,1,3,4,6,7,8,10,12,14};
		int[] column12S = new int[]{0,1,2,4,5,6,7,11,13,14};
		int[] column13S = new int[]{0,1,2,3,4,5,7,9,10,12,14};
		int[] column14S = new int[]{0,1,3,5,14};
		int[] column15S = new int[]{};

		maze.setColumnE(0, column0E);
		maze.setColumnE(1, column1E);
		maze.setColumnE(2, column2E);
		maze.setColumnE(3, column3E);
		maze.setColumnE(4, column4E);
		maze.setColumnE(5, column5E);
		maze.setColumnE(6, column6E);
		maze.setColumnE(7, column7E);
		maze.setColumnE(8, column8E);
		maze.setColumnE(9, column9E);
		maze.setColumnE(10, column10E);
		maze.setColumnE(11, column11E);
		maze.setColumnE(12, column12E);
		maze.setColumnE(13, column13E);
		maze.setColumnE(14, column14E);


		maze.setColumnS(0, column0S);
		maze.setColumnS(1, column1S);
		maze.setColumnS(2, column2S);
		maze.setColumnS(3, column3S);
		maze.setColumnS(4, column4S);
		maze.setColumnS(5, column5S);
		maze.setColumnS(6, column6S);
		maze.setColumnS(7, column7S);
		maze.setColumnS(8, column8S);
		maze.setColumnS(9, column9S);
		maze.setColumnS(10, column10S);
		maze.setColumnS(11, column11S);
		maze.setColumnS(12, column12S);
		maze.setColumnS(13, column13S);
		maze.setColumnS(14, column14S);
		maze.setColumnS(15, column15S);
	}

	public void setmaze(Maze maze) {

		paused = true;
		maze.mouse = new Mouse(0,0,Direction.NORTH);
		maze = new Maze(clearWalls(maze.walls), maze.mouse);
		maze.generateDefaultDistances(maze.walls);
		int[] column0E = new int[]{2,3,4,5,6,8,9,10,11,12,13,14,15};
		int[] column1E = new int[]{1,3,5,7,8,10,11};
		int[] column2E = new int[]{2,4,6,8,9,12,13,14};
		int[] column3E = new int[]{1,3,5,7,9,10,11,12,15};
		int[] column4E = new int[]{2,3,4,6,8,9,10,12,14};
		int[] column5E = new int[]{3,5,8,11,12,13};
		int[] column6E = new int[]{4,6,7,8,9,10};
		int[] column7E = new int[]{4,5,9,11};
		int[] column8E = new int[]{3,6,7,8,9,11,12,13};
		int[] column9E = new int[]{3,8,10,11,12};
		int[] column10E = new int[]{4,5,7,8,9,10,12,13,14};
		int[] column11E = new int[]{2,4,6,8,9,10,11,12,14};
		int[] column12E = new int[]{7,12,15};
		int[] column13E = new int[]{1,3,4,14};
		int[] column14E = new int[]{1,7,12,14};

		int[] column0S = new int[]{};
		int[] column1S = new int[]{0,1,2,4,6,10,13};
		int[] column2S = new int[]{0,7,10,12,14};
		int[] column3S = new int[]{0,2,3,4,5,6,9,11,13};
		int[] column4S = new int[]{0,1,6,7,12,13};
		int[] column5S = new int[]{0,2,5,10,14};
		int[] column6S = new int[]{0,1,2,4,6,8,9,11,12,13,14};
		int[] column7S = new int[]{0,1,2,3,6,10,14};
		int[] column8S = new int[]{0,1,2,4,6,8,11,12,13,14};
		int[] column9S = new int[]{0,1,2,4,6,8,11,14};
		int[] column10S = new int[]{0,1,2,4,5,10,13,14};
		int[] column11S = new int[]{0,1,3,5,6,12,14};
		int[] column12S = new int[]{0,1,2,3,3,5,7,9,11,13};
		int[] column13S = new int[]{0,2,4,6,8,9,10,12,13};
		int[] column14S = new int[]{1,2,3,5,7,8,9,10,11,13};
		int[] column15S = new int[]{3,4,7,8,10};

		maze.setColumnE(0, column0E);
		maze.setColumnE(1, column1E);
		maze.setColumnE(2, column2E);
		maze.setColumnE(3, column3E);
		maze.setColumnE(4, column4E);
		maze.setColumnE(5, column5E);
		maze.setColumnE(6, column6E);
		maze.setColumnE(7, column7E);
		maze.setColumnE(8, column8E);
		maze.setColumnE(9, column9E);
		maze.setColumnE(10, column10E);
		maze.setColumnE(11, column11E);
		maze.setColumnE(12, column12E);
		maze.setColumnE(13, column13E);
		maze.setColumnE(14, column14E);


		maze.setColumnS(0, column0S);
		maze.setColumnS(1, column1S);
		maze.setColumnS(2, column2S);
		maze.setColumnS(3, column3S);
		maze.setColumnS(4, column4S);
		maze.setColumnS(5, column5S);
		maze.setColumnS(6, column6S);
		maze.setColumnS(7, column7S);
		maze.setColumnS(8, column8S);
		maze.setColumnS(9, column9S);
		maze.setColumnS(10, column10S);
		maze.setColumnS(11, column11S);
		maze.setColumnS(12, column12S);
		maze.setColumnS(13, column13S);
		maze.setColumnS(14, column14S);
		maze.setColumnS(15, column15S);
	}

}